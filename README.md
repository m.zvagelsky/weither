# Use Credential Helper With JIB
View comments in the [jib.gradle](adapters/weather-api/jib.gradle) to learn how to use docker credentials helpers with jib.



# Modules

- [app](app) Main application module, exhibits public API and serves client requests using suitable data source via its REST API.
- [weather-api-server] Handle requests to the [https://www.weatherapi.com/](https://www.weatherapi.com/) data source.
