package ru.weather.requesters.http;

import lombok.extern.slf4j.Slf4j;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.time.Duration;

@Slf4j
public class JdkSender implements HttpSender {

    /** The client is thread-safe. */
    private final HttpClient client = HttpClient.newHttpClient();

    @Override
    public String get(String url) {
        var request = HttpRequest.newBuilder()
                .uri(URI.create(url))
                .timeout(Duration.ofSeconds(10))
                .build();
        try {
            var response = client.send(request, HttpResponse.BodyHandlers.ofString());
            return response.body();
        } catch (Exception ex) {
            if (ex instanceof InterruptedException) {
                Thread.currentThread().interrupt();
            }
            log.error("Http request error, url: {}", url, ex);
            throw new HttpSenderException(ex.getMessage());
        }
    }
}
