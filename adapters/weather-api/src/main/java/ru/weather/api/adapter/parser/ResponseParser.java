package ru.weather.api.adapter.parser;

import ru.weather.api.adapter.model.Weather;

public interface ResponseParser {
    Weather parse(String xml);
}
