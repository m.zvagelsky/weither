package ru.weather.api.adapter.config;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Data
@Component
public class AdapterProperties {

    @Value("${weather-api.api.json-url}")
    String jsonUrl;

    @Value("${weather-api.api.xml-url}")
    String xmlUrl;

    @Value("${weather-api.api.key}")
    String apiKey;

}
