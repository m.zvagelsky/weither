package ru.weather.api.adapter.parser;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import ru.weather.api.adapter.model.Weather;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.StringReader;

@Slf4j
@Service
public class W3cDomParser implements ResponseParser {

    @Override
    public Weather parse(String xml) {
        var factory = DocumentBuilderFactory.newInstance();
        try {
            factory.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);
            var builder = factory.newDocumentBuilder();
            try (var reader = new StringReader(xml)) {
                Document document = builder.parse(new InputSource(reader));
                Element root = document.getDocumentElement();
                root.normalize();
                /* Exact path: root.forecast.forecastday.day.avgtemp_c */
                NodeList tempElements = root.getElementsByTagName("avgtemp_c");
                /* TODO better handling of exceptional case */
                String temperature = tempElements.getLength() == 1
                        ? tempElements.item(0).getTextContent()
                        : "";
                return new Weather(temperature);
            }
        } catch (Exception e) {
            log.error("Exception when parsing weatherapi ");
            throw new RuntimeException(e);
        }
    }
}
