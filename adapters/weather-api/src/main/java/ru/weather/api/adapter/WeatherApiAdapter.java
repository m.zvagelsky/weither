package ru.weather.api.adapter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WeatherApiAdapter {
    public static void main(String[] args) {
        SpringApplication.run(WeatherApiAdapter.class);
    }
}
