package ru.weather.api.adapter.parser;

import org.junit.jupiter.api.Test;
import ru.weather.api.adapter.model.Weather;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;

import static org.assertj.core.api.Assertions.assertThat;

public class W3cDomParserTest {

    @Test
    void testParse() throws URISyntaxException, IOException {

        /* Read test xml file as a string.*/
        URI testFileUri = ClassLoader.getSystemResource("weather-api-response.xml").toURI();
        String testXml = Files.readString(Path.of(testFileUri));

        /* Parse test xml. */
        var parser = new W3cDomParser();
        Weather actualWeather = parser.parse(testXml);

        /* Check parsing result. */
        assertThat(actualWeather.getAirTemperature()).isEqualTo("17");
    }
}
