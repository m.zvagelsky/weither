package ru.weather.api.adapter.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.web.servlet.MockMvc;
import ru.weather.api.adapter.config.AdapterProperties;
import ru.weather.api.adapter.config.ApplicationConfig;
import ru.weather.api.adapter.config.JsonConfig;
import ru.weather.api.adapter.parser.W3cDomParser;
import ru.weather.api.adapter.services.WeatherApiService;
import ru.weather.requesters.http.HttpSender;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(WeatherController.class)
@Import({AdapterProperties.class, ApplicationConfig.class, JsonConfig.class, WeatherApiService.class, W3cDomParser.class})
public class WeatherControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private HttpSender mockHttpSender;

    @Value("${app.rest.api.prefix}")
    private String restApiPrefix;

    @Test
    @DirtiesContext
    void getWeatherTest() throws Exception {
        configureMockHttpSender();

        String response = mockMvc
                .perform(get(testUrl()))
                .andExpect(status().isOk())
                .andReturn().getResponse()
                .getContentAsString(StandardCharsets.UTF_8);
        assertThat(response).isEqualTo("{\"airTemperature\":\"17\"}");
    }

    @Test
    @DirtiesContext
    void cacheTest() throws Exception {
        configureMockHttpSender();
        mockMvc.perform(get(testUrl()));
        mockMvc.perform(get(testUrl()));
        /* Verify that sender is used only once, because the second request is served from a cache. */
        verify(mockHttpSender, times(1)).get(any());
    }

    private void configureMockHttpSender() throws IOException, URISyntaxException {
        URI testFileUri = ClassLoader.getSystemResource("weather-api-response.xml").toURI();
        String testXml = Files.readString(Path.of(testFileUri));
        when(mockHttpSender.get(any())).thenReturn(testXml);
    }

    private String testUrl() {
        String city = "London";
        String date = "2022-05-07";
        return String.format("%s/%s/%s", restApiPrefix, city, date);
    }
}
