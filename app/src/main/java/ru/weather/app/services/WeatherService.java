package ru.weather.app.services;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ru.weather.app.model.Weather;
import ru.weather.app.model.WeatherInformationSource;
import ru.weather.app.services.source.WeatherRequester;

import java.time.LocalDate;
import java.util.Map;

@Slf4j
@Service
@RequiredArgsConstructor
public class WeatherService {

    /**
     * Spring builds this map based on beans implementing the {@link WeatherRequester}
     * and their names specified at the bean's @Service annotation.
     */
    private final Map<String, WeatherRequester> requesters;

    public Weather getWeather(WeatherInformationSource source, String city, LocalDate date) {
        WeatherRequester requester = requesters.get(source.getRequesterName());
        Weather weather = requester.getWeather(city, date);
        return weather;
    }
}
