package ru.weather.app.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Data
@Component
@ConfigurationProperties(prefix = "weather-api")
public class WeatherAppProperties {
    String adapterUrl;
}
