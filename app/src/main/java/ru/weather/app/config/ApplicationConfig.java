package ru.weather.app.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ru.weather.requesters.http.JdkSender;
import ru.weather.requesters.http.HttpSender;

@Configuration
public class ApplicationConfig {

    @Bean
    public HttpSender httpSender() {
        return new JdkSender();
    }
}
