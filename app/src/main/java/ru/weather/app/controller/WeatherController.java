package ru.weather.app.controller;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.weather.app.model.Weather;
import ru.weather.app.model.WeatherInformationSource;
import ru.weather.app.services.WeatherService;

import java.time.LocalDate;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping(path = "/api/v1/weather")
public class WeatherController {

    private final WeatherService weatherService;

    @GetMapping(value = "/{source}/{city}/{date}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Weather getWeather(@PathVariable("source") WeatherInformationSource source,
                              @PathVariable("city") String city,
                              @DateTimeFormat(pattern = "yyyy-MM-dd") @PathVariable("date") LocalDate date) {
        log.info("getWeather, source: {}, city: {}, date: {}", source.name(), city, date);
        Weather weather = weatherService.getWeather(source, city, date);
        log.info("weather: {}", weather);
        return weather;
    }
}
