package ru.weather.app.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class Weather {
    String airTemperature;

    @JsonCreator
    public Weather(@JsonProperty("airTemperature") String airTemperature) {
        this.airTemperature = airTemperature;
    }
}
